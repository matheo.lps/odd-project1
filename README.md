# Need For Speed P1B
## Le jeu CSharp NFS-P1B propose des courses de "street race" et permet par la suite de remporter d'autres voitures au choix.

Membres du projet: Mathéo Lopes, Zackary Ioset, Matéo Piaulette

Déroulement: 
Lors de la première connexion: 
1. Demande du pseudonyme de l'utilisateur.
2. Choix de la première voiture parmis plusieurs de catégorie "Performences Basses".
_(3 catégories: BP -> Basses Performences, PM -> Performences Moyennes, HP -> Hautes Performences)_
3. Choix d'une course entre trois niveaux de difficultés : 250ch 500ch 800ch. 
_(Le choix de cette dificulté impactera à chaque fois un pourcentage de chance de gagner la course)_
3. Une fois la course terminée, les statistiques de la course sont affichées, ainsi que l'argent gagné.
4. De retour à la page d'accueil, celle-ci sera appelée le garage, il sera possible d'améliorer sa voiture, d'en acheter de nouvelles, et de relancer des courses.


